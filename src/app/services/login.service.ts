import { Injectable } from '@angular/core';
import { baseApi } from '../config/config';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class LoginService {

  loginUrl = baseApi + "User/GetUser";
  allUsersUrl = baseApi + "User/GetAllUsers";
  
  constructor(private http : HttpClient) {
  }

  loginUser( user ) : Promise<any> {
    return this.http.post(this.loginUrl, user)
    .toPromise()
    .then(response => response)
    .catch(this.catchHandler);
  }

  getAllUsers() : Promise<any> {
    return this.http.post(this.allUsersUrl, '')
    .toPromise()
    .then(response => response)
    .catch(this.catchHandler);
  } 

  isUserLoggedIn(){

      let loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
      if(loggedInUser != null){
        console.log("logged in true : ", loggedInUser)
          return true;
      }else{
        console.log("logged in false : ", loggedInUser)
        return false;
      }
  }


  catchHandler( error : HttpErrorResponse ){
      if(error.error instanceof ErrorEvent){
        console.log("An Error Occured", error.message);
      } 
  }
  
}
