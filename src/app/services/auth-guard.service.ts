import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { LoginService } from './login.service';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanDeactivate<DashboardComponent> {

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;
  //  console.log(url);
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
  //  console.log(url);
    if (this.loginService.isUserLoggedIn()) { 
      return true; 
    }
    else{
      this.router.navigate(['/login']);
      return false;
    }
  } 

  // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
  //   let url: string = state.url;
  //   return this.checkLogin(url);
  // }

  // canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
  //   return this.canActivate(route, state);
  // }

  canDeactivate(component: DashboardComponent): boolean {
    if (this.loginService.isUserLoggedIn()) { 
    //  console.log("Can Deactivate");
      //this.router.navigate(['/dashboard']);
      return false; 
    }
    else{
      return true;
    }
  }

}
