import { Injectable } from '@angular/core';
import { baseApi } from '../config/config';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import { Observable } from 'rxjs/Rx'

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  allProjectsUrl  = baseApi + "Project/GetAllProjects";
  allTestUrl = baseApi + "Test/GetAllTests";
  allMappingsUrl = baseApi + "LoginProject/GetAllMappings";
  getUserProjectsUrl = baseApi + "LoginProject/GetProjects";
  saveAuditUrl = baseApi + "AuditStatus/Insert";
  updateAuditUrl = baseApi + "AuditStatus/Update";
  getAllAuditsUrl = baseApi + "AuditStatus/GetAllMappings";
  getSelectedAuditUrl = baseApi + "AuditStatus/GetMapping"; 
  getUserTestsUrl = baseApi + "ProjectTest/GetTests";
  saveUnmappedProjectUrl = baseApi + "AuditStatus/AuditNoMap";
  

  constructor(private http : HttpClient) {
  }

  getAllProjects() : Promise<any> {
    return this.http.post(this.allProjectsUrl,'')
    .toPromise()
    .then(response => response)
    .catch(this.catchHandler);
  }

  getAllTests() : Promise<any> {
    return this.http.post(this.allTestUrl,'')
    .toPromise()
    .then(response => response)
    .catch(this.catchHandler);
  }

  getAllProjectMapping() : Promise<any> {
    return this.http.post(this.allMappingsUrl,'')
    .toPromise()
    .then(response => response)
    .catch(this.catchHandler);
  }

  getUserProjects(user) : Promise <any>{
    return this.http.post(this.getUserProjectsUrl, user)
    .toPromise()
    .then( response => response)
    .catch(this.catchHandler)
  }

  getProjectTests(projectId) : Promise <any>{
    return this.http.post(this.getUserTestsUrl, projectId)
    .toPromise()
    .then( response => response)
    .catch(this.catchHandler)
  }

  saveProjectAudit(auditObj : any): Promise<any> {
      return this.http.post(this.saveAuditUrl,auditObj)
      .toPromise()
      .then(response => response)
      .catch(this.catchHandler);
  }

  saveUnMappedProjectAudit(auditObj : any): Promise<any> {
    return this.http.post(this.saveUnmappedProjectUrl,auditObj)
    .toPromise()
    .then(response => response)
    .catch(this.catchHandler);
}

  updateProjectStatus( auditObj ): Promise<any> {
    return this.http.post(this.updateAuditUrl, auditObj)
    .toPromise()
    .then(response => response)
    .catch(this.catchHandler);
  }

  getAllProjectStatuses(): Promise<any> {
    return this.http.post(this.getAllAuditsUrl,'')
    .toPromise()
    .then(response => response)
    .catch()
  }
  
  getSelectedProjectStatus(projectObj): Promise<any> {
    return this.http.post(this.getSelectedAuditUrl,projectObj)
    .toPromise()
    .then(response => response)
    .catch()
  }

  catchHandler( error : HttpErrorResponse) {
      if(error.error instanceof ErrorEvent){
        console.log("An Error Occured", error.message);
      } 
  }
}
