import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { AuthGuardService as AuthGuard } from '../services/auth-guard.service';


const routes: Routes = [
  {path:'', redirectTo : 'dashboard',  pathMatch: 'full'},
  {
    path: '', component : PagesComponent, canActivate: [AuthGuard],  canDeactivate: [AuthGuard], children:[     
        { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'}
    ]
 }   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
