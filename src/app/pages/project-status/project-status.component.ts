import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-project-status',
  templateUrl: './project-status.component.html',
  styleUrls: ['./project-status.component.css']
})

export class ProjectStatusComponent implements OnInit {
  @Output() auditEvent = new EventEmitter<string>();
  
  @ViewChild('progressbar') progressbar;
  @ViewChild('zero') zero;
  @ViewChild('twenty') twenty;
  @ViewChild('fourty') fourty;
  @ViewChild('sixty') sixty;
  @ViewChild('eighty') eighty;
  @ViewChild('hundred') hundred;
  
  projectId = 0;
  testId = 0;
  isMappedProject = false;
  toggle_option = 'start';
  progress = 0;
  statusObject : any; 
  index=0;
  newParamFlag = false;
  isNewAudit = false;
  isNewUserProject = 'none';
  progressActiveStyle = "active";
  progressNormalStyle = "progressbar";
  selectedTestType : any;
  selectedProject : any;
  loggedInUser : any;
  selectedSupervisor : any;
  selectedTester : any;
  currentDate = new Date();
  existingAuditStatus : any;

  auditObject = {
    loginID : 0,
    projectID : 0,
    testID : 0,
    status : 0,
    supervisor: 0,
    auditBy : ""
  }

  constructor(private toastr: ToastrService, private projectService : ProjectService) {    
  }

  ngOnInit() {
    this.getStatus();
  }


  showSuccess() {
    this.toastr.success('Work progress submitted!', 'Status Updated!');
  }

  showFail() {
    this.toastr.error('Work progress not submitted!', 'Status Update Failed!');
  }

  getStatus(){

    this.progress = 0;
    this.startProject();
    this.selectedProject = JSON.parse(localStorage.getItem('selectedProject'));
    this.selectedTestType = JSON.parse(localStorage.getItem('selectedTest')); 
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    this.selectedSupervisor = JSON.parse(localStorage.getItem('selectedManager'));
    this.selectedTester = JSON.parse(localStorage.getItem('selectedTester'));
    this.isMappedProject = JSON.parse(localStorage.getItem('isMappedProject'));
    this.projectId = this.selectedProject.Project_ID;
    this.testId = this.selectedTestType.Test_ID;


    if(this.selectedTester !== null ){
      let status = {
        projectID: this.selectedProject.Project_ID ,
        testID: this.selectedTestType.Test_ID, 
        loginID : this.selectedTester.Login_ID
      }
  
      // console.log(status);
      this.projectService.getSelectedProjectStatus(status).then((response) => { 
      this.existingAuditStatus = response.recordset[0];

      // console.log(this.existingAuditStatus);

        if(this.existingAuditStatus != null || this.existingAuditStatus != undefined ){
          this.isNewAudit = false;
          this.updateProgressBar(this.existingAuditStatus);
        }else{
          this.isNewAudit = true;
        }
      });
    }
  }

  ngOnDestroy() {
  }

  submit(form){

    this.auditObject.status = this.progress;

    if(this.isMappedProject){
      this.isNewUserProject = 'none';
      this.submitAudit();
    }else{
      this.isNewUserProject = 'block';    
    }   
  }

  closeModal(){
    this.isNewUserProject = 'none';
  }

  submitAudit(){
   // console.log("Audit Object : ",this.auditObject);

    if(!this.isNewAudit){

        this.auditObject.loginID = this.selectedTester.Login_ID;
        this.auditObject.projectID = this.selectedProject.Project_ID;
        this.auditObject.testID = this.selectedTestType.Test_ID;
        this.auditObject.supervisor =  this.existingAuditStatus.Supervisor;
        this.auditObject.status = this.progress;
        this.auditObject.auditBy = this.loggedInUser.Name;
        if(this.progress == 0){
          this.auditObject.status = this.existingAuditStatus.STATUS;
        }else{
          this.auditObject.status = this.progress;
        }

      this.projectService.updateProjectStatus(this.auditObject).then( response => {
          let responseStatus = response.recordset[0];
            if(responseStatus[""] == 0){
              this.showFail();
            
            }else{
              this.showSuccess();
              this.auditSubmitted();
            }
      });
    }
    else{
      //  console.log('selectedManager');
        this.auditObject.loginID = this.selectedTester.Login_ID;
        this.auditObject.projectID = this.selectedProject.Project_ID;
        this.auditObject.testID = this.selectedTestType.Test_ID;
        this.auditObject.supervisor = this.selectedSupervisor.Login_ID;             //this.existingAuditStatus[0].Supervisor;
        this.auditObject.status = this.progress;
        this.auditObject.auditBy = this.loggedInUser.Name;

      this.projectService.saveProjectAudit(this.auditObject).then(response => {
        if(response.recordset != undefined){
          let responseStatus = response.recordset[0];
          if(responseStatus[""] == "Record Saved Successfully!!"){
            this.showSuccess();
            this.auditSubmitted();
          }else{
            this.showFail();
          }
        }else{
          this.showFail();
        }
      });
    }
  }

  submitUnmappedAudit(){
    this.auditObject.projectID = this.selectedProject.Project_ID;
    this.auditObject.testID = this.selectedTestType.Test_ID;
    this.auditObject.auditBy = this.loggedInUser.Name;
    this.auditObject.loginID = this.selectedTester.Login_ID;
    this.auditObject.supervisor = this.selectedSupervisor.Login_ID;
    this.auditObject.status = this.progress;

    this.projectService.saveUnMappedProjectAudit(this.auditObject).then(response => {

      if(response.recordset != undefined){
        let responseStatus = response.recordset[0];
      //  console.log(responseStatus[""]);
        if(responseStatus[""] == "Record Saved Successfully!!"){
          this.showSuccess();
          this.auditSubmitted();
        }else{
          this.showFail();
        }
      }else{
        this.showFail();
      }
    });
  }

  progressPercent(event){

    this.progress = event.target.value;
    if(this.progress == 100){
      this.toggle_option = 'closed';
      this.toggleClass(event, 'closed');
    }else{
      if(this.progress == 0){
        this.toggle_option = 'start';
        this.toggleClass(event, 'start');
      }
      else{
        this.toggle_option = 'wip';
        this.toggleClass(event, 'wip');
      }
    }
  }

  wipProgress(event){

      if(event.target.value == "start"){
          this.progress = 0;  
          this.startProject();
      }
      else{
        if(event.target.value == "closed"){
            this.progress = 100;
            this.closeProject();
        }
      }
  }

  toggleClass(event, status){

    if(status == 'wip'){
        if(event.target.value ==  20){
          this.progressTwenty();
        }
        if(event.target.value ==  40){
          this.progressFourty();
        }
        if(event.target.value ==  60){
            this.progressSixty();
        }
        if(event.target.value ==  80){
            this.progressEighty();
        }
        if(event.target.value ==  100){
              this.closeProject();
        }
    }else{
        if(status == 'closed'){
            this.closeProject();
        }
        else{
            this.startProject();
        }
    }
  }

  updateProgressBar(statusObject){

        if(statusObject === undefined){
          this.startProject();
        }else{
          if(statusObject.STATUS > 20){
            this.startProject();
          }
          if((statusObject.STATUS > 20  && statusObject.STATUS < 40) || statusObject.STATUS == 20){
               this.progressTwenty();
          }
          
          if((statusObject.STATUS > 40 && statusObject.STATUS < 60) ||  statusObject.STATUS == 40){
                this.progressFourty();
          }
          
          if(( statusObject.STATUS > 60 && statusObject.STATUS < 80) ||  statusObject.STATUS == 60){
                  this.progressSixty();
          }
             
          if( (statusObject.STATUS > 80 && statusObject.STATUS < 100) ||  statusObject.STATUS == 80){
                  this.progressEighty();
          }
                     
          if(statusObject.STATUS == 100){
            this.closeProject();
          }  
        }
  }

  auditSubmitted(){
    this.auditEvent.emit('mapped');
  }

  startProject(){
    this.toggle_option = 'start';
    this.zero.nativeElement.className = '';
    this.twenty.nativeElement.className = '';
    this.fourty.nativeElement.className ='';
    this.sixty.nativeElement.className = '';
    this.eighty.nativeElement.className = '';
  }

  progressTwenty(){
    this.toggle_option = 'wip';
    this.zero.nativeElement.className = 'active';
    this.twenty.nativeElement.className = '';
    this.fourty.nativeElement.className ='';
    this.sixty.nativeElement.className = '';
    this.eighty.nativeElement.className = '';
  }
  
  progressFourty(){
    this.toggle_option = 'wip';
    this.zero.nativeElement.className = 'active';
    this.twenty.nativeElement.className = 'active';
    this.fourty.nativeElement.className ='';
    this.sixty.nativeElement.className = '';
    this.eighty.nativeElement.className = '';
  }

  progressSixty(){
    this.toggle_option = 'wip';
    this.zero.nativeElement.className = 'active';
    this.twenty.nativeElement.className = 'active';
    this.fourty.nativeElement.className = 'active';
    this.sixty.nativeElement.className = '';
    this.eighty.nativeElement.className = '';
  }

  progressEighty(){
    this.toggle_option = 'wip';
    this.zero.nativeElement.className = 'active';
    this.twenty.nativeElement.className = 'active';
    this.fourty.nativeElement.className = 'active';
    this.sixty.nativeElement.className = 'active';
    this.eighty.nativeElement.className = '';
  }

  closeProject(){
    this.toggle_option = 'closed';
    this.zero.nativeElement.className = 'active';
    this.twenty.nativeElement.className = 'active';
    this.fourty.nativeElement.className ='active';
    this.sixty.nativeElement.className = 'active';
    this.eighty.nativeElement.className = 'active';
  }

  TwentyLess(){
    this.toggle_option = 'wip';
    this.zero.nativeElement.className = '';
    this.twenty.nativeElement.className = '';
    this.fourty.nativeElement.className ='';
    this.sixty.nativeElement.className = '';
    this.eighty.nativeElement.className = '';
  }

}
