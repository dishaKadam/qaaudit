import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectStatusRoutingModule } from './project-status-routing.module';
import { ProjectStatusComponent } from './project-status.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectService } from '../../services/project.service';

@NgModule({
  imports: [
    CommonModule,
    ProjectStatusRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ProjectStatusComponent],
  exports: [ProjectStatusComponent],
  providers: [ ProjectService]
})
export class ProjectStatusModule { }
