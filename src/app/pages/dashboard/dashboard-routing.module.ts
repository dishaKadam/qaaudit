import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
    { path: '', component: DashboardComponent }
    //     path: '', component: DashboardComponent, children:[
    //         { path: 'employee-selection/:userType', loadChildren: '../employee-selection/employee-selection.module#EmployeeSelectionModule'},
    //         { path: 'project-status/:projectId/:testId/:isMappedProject', loadChildren: '../project-status/project-status.module#ProjectStatusModule'},
    //         { path: 'project-status', loadChildren: '../project-status/project-status.module#ProjectStatusModule'} 
    //     ]
    // }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
