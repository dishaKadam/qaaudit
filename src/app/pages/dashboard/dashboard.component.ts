import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from '../../services/project.service';
import { EmployeeSelectionComponent } from '../employee-selection/employee-selection.component';
import { ProjectStatusComponent } from "../project-status/project-status.component";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  @ViewChild('employee') employeeComp : EmployeeSelectionComponent;
  @ViewChild('audit') auditComp : ProjectStatusComponent;
  @ViewChild('logOut') logOut;
  @ViewChild('allProjects') allProjects;

  loginProjectList : any[] = [];
  projectList : any[] = [];
  testList : any[] = [];
  projectFlag = false;
  previousSelectedTest : any;
  previousSelectedProject : any;
  selectedUserType : any;
  previousSelectedTester : any;
  showAllProjectsFlag = false;
  isMappedProject = false;
  loggedInProject = false;
  isAuditFlag = false;
  isEmployeeFlag = true;
  selectedTester : any;
  selectedManager : any;
  loggedInUser : any;
  
  userlogin = {
    loginID : 0
  }

  constructor(public router : Router, private projectService : ProjectService, private toastr : ToastrService) {
      this.projectService.getAllProjects().then(response => {
        this.projectList = response.recordset;
      })

      this.projectService.getAllTests().then(response => {
          this.testList = response.recordset;
      })

      this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    //  console.log("Logged In User : ",this.loggedInUser);

    localStorage.removeItem('isManagerSelected');
    localStorage.removeItem('isTesterSelected');
    localStorage.removeItem('selectedTester');
    localStorage.removeItem('selectedManager');
    localStorage.removeItem('selectedProject');
    localStorage.removeItem('selectedTest');
    localStorage.removeItem('userType');
   }

  ngOnInit() {
  }

  selectEmployee(userType : string) { 
      this.isEmployeeFlag = true;
      this.isAuditFlag = false;
      // console.log("User Type  : ", userType);
      this.selectedUserType = userType;
      localStorage.setItem('userType', JSON.stringify(userType));
      if(this.employeeComp ==  undefined){}
      else{
        // this.employeeComp.userTypeSelected(userType);
        this.employeeComp.userTypeSelected();
      }
  }

  progressStatus(selection : string, id, element, selectedItem) {

    if(((localStorage.getItem('isTesterSelected') == 'true') && (localStorage.getItem('isManagerSelected') == 'true'))){
     
      if(selection == 'project'){

        localStorage.removeItem('selectedTest');
        this.isAuditFlag = false;
       
              if(this.previousSelectedProject == undefined){
                element.className = "selected_option";
                this.previousSelectedProject = element;

                  const projectExist = this.loginProjectList.find( project => 
                      project.Project_ID  === selectedItem.Project_ID
                  )

                  if(projectExist != undefined){
                      this.isMappedProject = true;
                  }else{
                    this.isMappedProject = false;
                  }
              }
              else{
                this.previousSelectedProject.className = "button";
                element.className = "selected_option";
                this.previousSelectedProject = element;

                const projectExist = this.loginProjectList.find( project => 
                  project.Project_ID  === selectedItem.Project_ID
                )

                if(projectExist != undefined){
                    this.isMappedProject = true;
                }else{
                  this.isMappedProject = false;
                }
              }
              
              localStorage.setItem('selectedProject', JSON.stringify(selectedItem));
              this.navigateToProjectStatus();
            //  console.log("Selected Project in dashboard :", JSON.parse(localStorage.getItem('selectedProject')))
              this.projectFlag = true;

              let projectId = {
                projectID : selectedItem.Project_ID
              }

              this.projectService.getProjectTests(projectId).then(response => {
                this.testList = response.recordset;
              })

            }else{
                if( this.projectFlag){
                  if(this.previousSelectedTest == undefined){
                    element.className = "selected_option";
                    this.previousSelectedTest = element;
                  }
                  else{
                    this.previousSelectedTest.className = "button";
                    element.className = "selected_option";
                    this.previousSelectedTest = element;
                  }
                  localStorage.setItem('selectedTest', JSON.stringify(selectedItem));
                  this.navigateToProjectStatus();
                //  console.log("Selected Test in dashboard:  ", JSON.parse(localStorage.getItem('selectedTest')));
                } 
                else{
                  this.showInfo('Please Select A Project!');
                } 
            }
          }
          else{
            if(localStorage.getItem('isTesterSelected') == '' || localStorage.getItem('isTesterSelected') == null){
              this.showInfo('Please Select A Staff!');
            }
           else{
              this.showInfo('Please Select A Lead!');
            }
          }
    }
    
    showAllProjects(){
      localStorage.removeItem('selectedProject');
      localStorage.removeItem('selectedTest');
        this.showAllProjectsFlag = true;
      //  console.log(this.allProjects);
       this.allProjects.nativeElement.className = 'fa fa-file-text-o fa-3x icons-active';
        this.projectService.getAllProjects().then(response => {
          this.projectList = response.recordset;
        });
        this.projectService.getAllTests().then(response => {
          this.testList = response.recordset;
      });
      this.isAuditFlag = false;
    }
    
  showInfo(message) {
    this.toastr.info(message, 'Info');
  }

  navigateToProjectStatus(){
    this.selectedUserType = '';
    let project =  JSON.parse(localStorage.getItem('selectedProject'));
    let test =  JSON.parse(localStorage.getItem('selectedTest'));

    // console.log("Project on clearance ",project);
    // console.log("Test On clearance ",test);

    if((project == undefined || test == undefined)||(project == null || test == null)){}
    else{
      this.isEmployeeFlag = false;
      this.isAuditFlag = true;
      if(this.isMappedProject){
        localStorage.setItem('isMappedProject', 'true');
          if(this.auditComp==undefined){
          }else{
            this.auditComp.getStatus();
          }
      }else{
        localStorage.setItem('isMappedProject', 'false');
        if(this.auditComp==undefined){
        }else{
          this.auditComp.getStatus();
        }
      }
    }
  }

  userSelected(userEvent) {
    if(userEvent == 'tester'){
       this.selectedTester = JSON.parse(localStorage.getItem('selectedTester'));
      let testerLoginId = { loginID : this.selectedTester.Login_ID }
        this.projectService.getUserProjects(testerLoginId).then( response => {    
          this.projectList = response.recordset;
          this.loginProjectList = response.recordset;
        }); 
    }else{ 
      this.selectedManager = JSON.parse(localStorage.getItem('selectedManager'));
    }
  }

  auditSubmitted(auditEvent){
   // console.log("User Selected From Child : ", auditEvent);
    let selectedTester = JSON.parse(localStorage.getItem('selectedTester'));
      let testerLoginId = { loginID : selectedTester.Login_ID }
        this.projectService.getUserProjects(testerLoginId).then( response => {    
          this.projectList = response.recordset;
          this.loginProjectList = response.recordset;
        }); 
        this.projectService.getAllTests().then(response => {
            this.testList = response.recordset;
        })
        this.isAuditFlag = false;
        this.showAllProjectsFlag = false;
        this.allProjects.nativeElement.className = 'fa fa-file-text-o fa-3x';
        localStorage.removeItem('selectedProject');
        localStorage.removeItem('selectedTest'); 
  }

  logout(){
    this.logOut.nativeElement.className = 'fa fa-file-text-o fa-3x icons-active';
    localStorage.removeItem('isManagerSelected');
    localStorage.removeItem('isTesterSelected');
    localStorage.removeItem('selectedProject');
    localStorage.removeItem('selectedTest');
    localStorage.removeItem('loggedInUser');
    localStorage.removeItem('selectedTester');
    localStorage.removeItem('selectedManager');
    this.router.navigate(['/login']);
  }
}
