export const testType : any[] = [

    {testTypeId : 1, testTypeName : 'BaseLine Testing'},
    {testTypeId : 2, testTypeName : 'Regression Testing'},
    {testTypeId : 3, testTypeName : 'GUI Testing'},
    {testTypeId : 4, testTypeName : 'Load Testing'},
    {testTypeId : 5, testTypeName : 'Smoke Testing'},
    {testTypeId : 6, testTypeName : 'Automation Testing'},
    {testTypeId : 7, testTypeName : 'Monkey Testing'},
    {testTypeId : 8, testTypeName : 'Interface Testing'},
    {testTypeId : 9, testTypeName : 'Boundary Testing'},
    {testTypeId : 10, testTypeName : 'Workflow Test'},
    {testTypeId : 11, testTypeName : 'Negative Testing'},
    {testTypeId : 12, testTypeName : 'Integration Testing'},
    {testTypeId : 13, testTypeName : 'Variable CCY Testing'},
    {testTypeId : 14, testTypeName : 'Security Testing'},
    {testTypeId : 15, testTypeName : 'Calculations'},
    {testTypeId : 16, testTypeName : 'Stress Testing'},
    {testTypeId : 17, testTypeName : 'Triangulation'},
    {testTypeId : 18, testTypeName : 'Testing On Slow DB'},
    {testTypeId : 19, testTypeName : 'DB Comparison'},
    {testTypeId : 20, testTypeName : 'Browser Compatibility Testing'}
    
]