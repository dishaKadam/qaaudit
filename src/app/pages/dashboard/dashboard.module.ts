import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ProjectService } from '../../services/project.service';
import { EmployeeSelectionComponent } from '../employee-selection/employee-selection.component';
import { ProjectStatusComponent } from '../project-status/project-status.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [DashboardComponent, EmployeeSelectionComponent, ProjectStatusComponent],
  providers: [ProjectService]
})

export class DashboardModule { }
