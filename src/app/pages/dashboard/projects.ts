export const projects: any[] = [

        {projectId : 1, projectName: 'ANZ'},
        {projectId : 2, projectName: 'BBL'},
        {projectId : 3, projectName: 'BOCI'},
        {projectId : 4, projectName: 'BOCM'},
        {projectId : 5, projectName: 'CMB'},
        {projectId : 6, projectName: 'CS'},
        {projectId : 7, projectName: 'DBS'},
        {projectId : 8, projectName: 'DB'},
        {projectId : 9, projectName: 'EQC'},
        {projectId : 10, projectName: 'FNQ'},
        {projectId : 11, projectName: 'HLB'},
        {projectId : 12, projectName: 'MBB'},
        {projectId : 13, projectName: 'OCB'},
        {projectId : 14, projectName: 'RHB'},
        {projectId : 15, projectName: 'SCB'},
        {projectId : 16, projectName: 'UBS'},
        {projectId : 17, projectName: 'UOB'},
        {projectId : 18, projectName: 'UOB FX'},
        {projectId : 19, projectName: 'UOB RIMS'},
        {projectId : 20, projectName: 'WLB'}

]