import { EmployeeSelectionModule } from './employee-selection.module';

describe('EmployeeSelectionModule', () => {
  let employeeSelectionModule: EmployeeSelectionModule;

  beforeEach(() => {
    employeeSelectionModule = new EmployeeSelectionModule();
  });

  it('should create an instance', () => {
    expect(employeeSelectionModule).toBeTruthy();
  });
});
