import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeSelectionComponent } from './employee-selection.component';

const routes: Routes = [
  {path: '', component : EmployeeSelectionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeSelectionRoutingModule { }
