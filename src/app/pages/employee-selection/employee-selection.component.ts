import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../../services/login.service';


@Component({
  selector: 'app-employee-selection',
  templateUrl: './employee-selection.component.html',
  styleUrls: ['./employee-selection.component.css']
})

export class EmployeeSelectionComponent implements OnInit, OnDestroy {

  @Output() employeeSelected = new EventEmitter<string>();

  testersList : any[];
  managersList : any[];
  usersList : any[];
  managerFlag = false;
  testerFlag = false;
  selectedTester : any;
  selectedManager : any;
  //displayImage : any;

  constructor(private loginService : LoginService) { 
        this.loginService.getAllUsers().then(response => {
          this.usersList = response.recordset;
         // this.createHexString(response.recordset);
        // console.log(this.usersList);
        })
        this.selectedTester =  JSON.parse(localStorage.getItem('selectedTester'));
  }

  ngOnInit() {
    this.userTypeSelected();
  }
  
  userTypeSelected(){   
    let userType =  JSON.parse(localStorage.getItem('userType'));  
    if(userType == 'tester'){
      this.testerFlag = true;
      this.managerFlag = false;
    }else{
      if(userType == 'manager'){
        this.managerFlag = true;
        this.testerFlag = false;
      }else{}
    }
  }

  ngOnDestroy(){
  }

  selectTester(tester){
     this.selectedTester = tester;     // CSS Class Application to selected User
     localStorage.setItem('isTesterSelected','true');   
      localStorage.setItem('selectedTester', JSON.stringify(tester));
      localStorage.removeItem('userType');
      this.employeeSelected.emit('tester');
  }

  selectManager(manager){
    this.selectedManager = manager;     // CSS Class Application to selected User
    localStorage.setItem('isManagerSelected','true');
    localStorage.setItem('selectedManager', JSON.stringify(manager));
    localStorage.removeItem('userType');
    this.employeeSelected.emit('manager');
  }

  // createHexString(usersArray){                     // Converting integer array buffers to base64.
    
  //   for(let i = 0; i < usersArray.length; i++) {
  //     let temp1 = usersArray[i].Photo ;
  //     if(temp1 == null || temp1 == undefined){
  //     }else{
  //       let temp2 = temp1['data'];
  //       if(temp2 != undefined ){
          
  //         this.usersList[i].Photo = this.base64ArrayBuffer(temp2);
  //       }
  //       else{
  //       }
  //     }
  //   }
  // }
  

  // base64ArrayBuffer(arrayBuffer) 
  // {
  //   var base64    = ''
  //   var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

  //   var bytes         = new Uint8Array(arrayBuffer)
  //   var byteLength    = bytes.byteLength
  //   var byteRemainder = byteLength % 3
  //   var mainLength    = byteLength - byteRemainder

  //   var a, b, c, d
  //   var chunk

  //   // Main loop deals with bytes in chunks of 3
  //   for (var i = 0; i < mainLength; i = i + 3) {
  //     // Combine the three bytes into a single integer
  //     chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

  //     // Use bitmasks to extract 6-bit segments from the triplet
  //     a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
  //     b = (chunk & 258048)   >> 12 // 258048   = (2^6 - 1) << 12
  //     c = (chunk & 4032)     >>  6 // 4032     = (2^6 - 1) << 6
  //     d = chunk & 63               // 63       = 2^6 - 1

  //     // Convert the raw binary segments to the appropriate ASCII encoding
  //     base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
  //   }

  //   // Deal with the remaining bytes and padding
  //   if (byteRemainder == 1) {
  //     chunk = bytes[mainLength]

  //     a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

  //     // Set the 4 least significant bits to zero
  //     b = (chunk & 3)   << 4 // 3   = 2^2 - 1

  //     base64 += encodings[a] + encodings[b] + '=='
  //   } else if (byteRemainder == 2) {
  //     chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

  //     a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
  //     b = (chunk & 1008)  >>  4 // 1008  = (2^6 - 1) << 4

  //     // Set the 2 least significant bits to zero
  //     c = (chunk & 15)    <<  2 // 15    = 2^4 - 1

  //     base64 += encodings[a] + encodings[b] + encodings[c] + '='
  //   } 
  //   return base64
  // }

}
