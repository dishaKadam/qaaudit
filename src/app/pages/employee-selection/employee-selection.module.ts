import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeSelectionRoutingModule } from './employee-selection-routing.module';
import { EmployeeSelectionComponent } from './employee-selection.component';
import { LoginService } from '../../services/login.service';

@NgModule({
  imports: [
    CommonModule,
    EmployeeSelectionRoutingModule
  ],
  declarations: [EmployeeSelectionComponent],
  exports: [EmployeeSelectionComponent],
  providers: [LoginService]
})
export class EmployeeSelectionModule { }
