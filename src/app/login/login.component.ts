import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
//import {Md5} from 'ts-md5/dist/md5';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;
 // md5 = new Md5();

  constructor( private router : Router, private loginService : LoginService, private formBuilder : FormBuilder, private toastr: ToastrService) {      
    this.initializeForm();         
   }

   initializeForm(){

     this.loginForm = this.formBuilder.group({
       userName : ['',  Validators.compose([Validators.required])],
       password : ['',  Validators.compose([Validators.required])]
     })
   }

  ngOnInit() {
   
  }

  login(form){

        this.loginService.loginUser(form).then(response => {
            let user = response.recordset;
            if((user[0] != undefined || user[0] != null) && (user[0].Auditor_Flag == 1)) {
              console.log(user[0]);
              localStorage.setItem('loggedInUser', JSON.stringify(user[0]));
              console.log("Logged In User : ", JSON.parse(localStorage.getItem('loggedInUser')));
              this.router.navigate(['/dashboard']);
              this.showSuccess(form.userName);
            //  console.log(Md5.hashStr(form.password));
            }else{
                this.showFailure();
            }
          },
          error =>{
            console.log(error);
          }
        )
  }

  showSuccess(name) {
    this.toastr.success('Logged in as '+name, 'Login Successfull!');
  }

  showFailure() {
    this.toastr.error('Please Check UserName And Password', 'Login Failed!');
  }
}
