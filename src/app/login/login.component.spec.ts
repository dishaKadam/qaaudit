import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { LoginService } from '../services/login.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {Injector} from "@angular/core";
import { ToastrService } from 'ngx-toastr';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let injector:  Injector;
  let loginService : LoginService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports:[FormsModule, ReactiveFormsModule, RouterTestingModule, HttpClientModule, HttpClientTestingModule],
      providers: [LoginService, ToastrService]
    })
    .compileComponents();

    injector = getTestBed();
    loginService = injector.get(LoginService);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
